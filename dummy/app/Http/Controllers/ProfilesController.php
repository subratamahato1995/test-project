<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class ProfilesController extends Controller
{

    public function __construct(){
        //$this->middleware('auth');
    }

    public function index(\App\User $user)
    {

        $follows = (auth()->user()) ? auth()->user()->following->contains($user->id) : false;
        //dd(Auth::user()->id);
        //dd($user->profile->id);
        //$user = User::findOrFail($user);
        //dd($follows);

        if(!isset(Auth::user()->id))
            return view('profiles.index', [
                'user' => $user,  
                'id'  => $user->id,    
                        
            ]);

        return view('profiles.index', [
            'user' => $user,  
            'id'  => $user->id,
            'auth' => Auth::user()->id,
            'follows' => $follows,
        ]);

        //dd(User::find($user));       
    }

    public function edit(\App\User $user){

        $this->authorize('update', $user->profile);
        return view('profiles.edit', compact('user'));

    }

    public function update(User $user){
        $this->authorize('update', $user->profile);

        $data = request()->validate([
            'title' => 'required',
            'description' => 'required',
            'url' => 'url',
            'image' => 'image',
        ]);

        
        if(request('image')){
            $imagePath = request('image')->store('uploads', 'public');
            
                       
        }  

        //$data = ['image' => $imagePath];

        //Auth::user()->profile->update($data);
        
        //dd($imagePath);
        
        Auth::user()->profile->update(array_merge(
            $data,
            ['image' => $imagePath]
         ));

        return redirect("/profile/{$user->id}");
    }
}
