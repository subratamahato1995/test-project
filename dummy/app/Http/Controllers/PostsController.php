<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{

    public function __construct(){
        $this->middleware('auth', ['except' => ['welcome', 'show']]);
    }
          
    
    public function create(){
        return view('posts.create');
    }

    public function store(){
        $data = request()->validate([
            'caption' => 'required',
            'image' => ['required', 'image'],
        ]);



        
        
        $imagePath = request('image')->store('uploads', 'public');

        \App\Post::create([
            'caption' =>$data['caption'],
            'image' =>$imagePath,
            'user_id' => Auth::user()->id,

        ]);

        return redirect('/profile/'. Auth::user()->id);
    }

    public function show(\App\Post  $post){
        return view('posts.show', compact('post'));
    }

    public function welcome(){
        $data = new Post();
        //$data['post'] = $data->get();
        $posts = $data->paginate(6);
        //dd($data);
        return view('welcome', compact('posts'));
        
    }

}
