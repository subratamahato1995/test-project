@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row d-flex justify-content-between">
    @if(isset($auth))
        @if ($auth == $id)
        <div class=" justify-content-end "><a href="/p/create" class="btn btn-lg btn-primary ">CREATE POST</a></div>
        @endif
        
        @if ($auth == $id)      
        <div class=" justify-content-end "><a href="/profile/{{$user->id}}/edit" class="btn btn-lg btn-secondary ">EDIT PROFILE</a></div>
        @endif
    @endif
    </div>
    <div class="row">
        <div class="col-3 p-5">
            <img src="/storage/{{$user->profile->image}}" alt=""  class="w-100">
        </div>
        <div class="col-9 pt-5">

            <div class="d-flex">
                    <div class="h1"> {{$user->username}}  </div>
                   
                    @if(isset($auth) && $auth != $id) 
            <follow-button @click="followUser" user-id="{{$user->id}}" follows="{{ $follows }}"></follow-button>
                    @endif
            </div>

            <div class="d-flex" >
                <div class="pr-3"><strong>{{$user->posts->count()}} </strong> posts</div>
                <div class="pr-3"><strong> {{$user->profile->followers->count()}}</strong> followers</div>
                <div class="pr-3"><strong>{{$user->following->count()}}</strong> following</div>
            </div>
            <div class="pt-3">
            <div class="h4">{{$user->profile->title}}</div>
            <p>{{$user->profile->description}}</p>
            <a href="{{$user->profile->url}}">{{$user->profile->url}}</a>
            </div>
        </div>
    </div>
    
    <div class="row pt-5">

        @foreach ($user->posts as $post)
            
        
        <div class="col-4 p-2">

        <a href="/p/{{$post->id}}"><img src="/storage/{{ $post->image}}" alt="" height="300"  class="w-100"></a>
        
        </div>
        @endforeach
        

    </div>





</div>
@endsection
