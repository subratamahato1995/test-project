@extends('layouts.app')

@section('content')
<div class="container">
        <div class="row ">

                @foreach ($posts as $post)
                    
                
                <div class="col-4 p-2">
        
                    <a href="/p/{{$post->id}}"><img src="/storage/{{ $post->image}}" alt="" height="300"  class="w-100"></a>
                
                </div>
                @endforeach
                
        
            </div>
            


            
            <div class="row pt-5  d-flex justify-content-center">
               
                <div class="col-3 ">
                    {{ $posts->links() }}
                </div>
           
        </div>


</div>
@endsection
