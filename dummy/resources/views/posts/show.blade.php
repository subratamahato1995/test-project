@extends('layouts.app')

@section('content')
<div class="container">
    
<div class="row pt-5">
    <div class="col-8">
        <img src="\storage\{{$post->image}}" alt="" class="w-100">
    </div>

    <div class="col-4">
    <h2 class="display-3"><a href="/profile/{{$post->user->id}}">{{$post->user->username}}</a></h2>
    <p class="text-secondary">
        {{$post->caption}}
    </p>
   
    </div>
</div>


</div>
@endsection
